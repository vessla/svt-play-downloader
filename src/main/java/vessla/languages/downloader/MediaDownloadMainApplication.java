package vessla.languages.downloader;

import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import vessla.languages.downloader.exception.DownloadInProgressException;
import vessla.languages.downloader.exception.UnableToFetchStreamInfoException;
import vessla.languages.downloader.stream.info.StreamInfo;
import vessla.languages.downloader.stream.info.StreamInfo.StreamType;
import vessla.languages.downloader.svt.SvtMediaDownloadTaskManager;
import vessla.languages.downloader.task.MediaDownloadTaskListener;
import vessla.languages.downloader.task.MediaDownloadTaskManager;
import vessla.languages.downloader.task.event.MediaDownloadCompleteEvent;
import vessla.languages.downloader.task.event.MediaDownloadErrorEvent;

public class MediaDownloadMainApplication {
	
	private static final Logger logger = Logger.getLogger(MediaDownloadMainApplication.class);
	
    public static void main(String[] args) {
    	
    	MediaDownloadCommandLineOptions options = new MediaDownloadCommandLineOptions();
    	
    	CmdLineParser parser = new CmdLineParser(options);
        try {
                parser.parseArgument(args);
                
                //TODO: Future work - allow dynamic loading of different downloaders
                MediaDownloadTaskManager downloadManager = new SvtMediaDownloadTaskManager();
                
                fetchStreamInfo(downloadManager, options.getSourceStreamUrl());
        		
                if(options.getStreamIndex() != null){
                	downloadStream(downloadManager, options.getStreamType(), options.getStreamIndex(), options.getTargetFileName());
                }
                else if (options.getStreamType() != null){
                	displayStreamInfo(downloadManager, options.getStreamType());
                }
                else{
                	displayStreamInfo(downloadManager);
                }
                
        } catch (CmdLineException e) {
        	logger.debug(e.getMessage());
            parser.printUsage(System.err);
        }
   
    }
    
    private static void fetchStreamInfo(MediaDownloadTaskManager downloadManager, String streamUrl){
 		System.out.println("Fetching stream info...");
 		try {
			downloadManager.fetchStreamInfo(streamUrl);
		} catch (UnableToFetchStreamInfoException e1) {
			System.out.println("Fetching stream info failed...");
			logger.debug(e1.getCause());
		}
		System.out.println("Fetching stream info complete!");  
    }
    
    private static void downloadStream(MediaDownloadTaskManager downloadManager, StreamType streamType, Integer streamIndex, String targetFile){
    	try {            
    		System.out.println("Download process started...");
			ArrayList<StreamInfo> streams = downloadManager.getAvailableStreams(streamType);
			downloadManager.download(streams.get(streamIndex), targetFile, new MediaDownloadTaskListener() {
				
				@Override
				public void onDownloadError(MediaDownloadErrorEvent e) {
					System.out.println("Stream download failed...");
				}
				
				@Override
				public void onDownloadComplete(MediaDownloadCompleteEvent e) {
					System.out.println("Stream download completed successfully!");
				}
			});
			
		} catch(DownloadInProgressException e){
			System.out.println("Already busy with downloading other stream...");
		} catch(FileAlreadyExistsException e){
			System.out.println("The given target file already exists...");
		}
    }
    
    private static void displayStreamInfo(MediaDownloadTaskManager downloadManager, StreamType streamType){
    	ArrayList<StreamInfo> streams = downloadManager.getAvailableStreams(streamType);
		for(StreamInfo info:streams){
			System.out.println(info.getDescription());
		}	
    }
    
    private static void displayStreamInfo(MediaDownloadTaskManager downloadManager){
    	ArrayList<StreamInfo> streams;
		for(StreamType type: StreamType.values()){
			streams = downloadManager.getAvailableStreams(type);
			System.out.println(type+" streams: ");
			for(StreamInfo info:streams){
				System.out.println(info.getDescription());
			}	
		}
    }

}
