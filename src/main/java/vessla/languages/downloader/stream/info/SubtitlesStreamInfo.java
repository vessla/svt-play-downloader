package vessla.languages.downloader.stream.info;

import java.math.BigInteger;

public class SubtitlesStreamInfo extends StreamInfo{
	
	private String url;
	
	public SubtitlesStreamInfo(String description, StreamType type, String url, BigInteger partCount){
		super(description, type, partCount);
		this.url = url;
	}
	
	//-----------------------------------------------------------------------------------------------------
	// Getters and setters
	//-----------------------------------------------------------------------------------------------------
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
