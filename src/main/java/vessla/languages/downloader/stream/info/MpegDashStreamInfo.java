package vessla.languages.downloader.stream.info;

import java.math.BigInteger;

import vessla.languages.downloader.stream.description.mpd.MPDtype;

public class MpegDashStreamInfo extends StreamInfo{
	
	private MPDtype mpd;
	private String baseUrl;
	private int adaptationSetIndex;
	private int representationIndex;

	public MpegDashStreamInfo(String description, StreamType type, MPDtype mpd, int adaptationSetIndex, int representationIndex, String baseUrl, BigInteger partCount) {
		super(description, type, partCount);
		this.mpd = mpd;
		this.adaptationSetIndex = adaptationSetIndex;
		this.representationIndex = representationIndex;
		this.baseUrl = baseUrl;
	}
	
	//-----------------------------------------------------------------------------------------------------
	// Getters and setters
	//-----------------------------------------------------------------------------------------------------

	public MPDtype getMpd() {
		return mpd;
	}

	public void setMpd(MPDtype mpd) {
		this.mpd = mpd;
	}

	public int getAdaptationSetIndex() {
		return adaptationSetIndex;
	}

	public void setAdaptationSetIndex(int adaptationSetIndex) {
		this.adaptationSetIndex = adaptationSetIndex;
	}

	public int getRepresentationIndex() {
		return representationIndex;
	}

	public void setRepresentationIndex(int representationIndex) {
		this.representationIndex = representationIndex;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public String getBaseUrl() {
		return baseUrl;
	}

}
