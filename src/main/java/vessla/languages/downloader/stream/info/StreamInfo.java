package vessla.languages.downloader.stream.info;

import java.math.BigInteger;

public abstract class StreamInfo {
	
	public enum StreamType{VIDEO, AUDIO, AUDIOVIDEO, SUBTITLES}

	private String description;
	private StreamType type;
	private BigInteger partCount;
	
	public StreamInfo(String description, StreamType type, BigInteger partCount){
		this.description = description;
		this.type = type;
		this.partCount = partCount;
	}
	
	//-----------------------------------------------------------------------------------------------------
	// Getters and setters
	//-----------------------------------------------------------------------------------------------------
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public StreamType getType() {
		return type;
	}
	public void setType(StreamType type) {
		this.type = type;
	}

	public BigInteger getPartCount() {
		return partCount;
	}

	public void setPartCount(BigInteger partCount) {
		this.partCount = partCount;
	}

}
