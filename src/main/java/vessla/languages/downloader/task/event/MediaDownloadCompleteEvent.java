package vessla.languages.downloader.task.event;

public class MediaDownloadCompleteEvent extends MediaDownloadEvent{

	private boolean isFullStreamDownloaded;
	
	public MediaDownloadCompleteEvent(boolean isFullStreamDownloaded){
		this.isFullStreamDownloaded = isFullStreamDownloaded;
	}

	public boolean isFullStreamDownloaded() {
		return isFullStreamDownloaded;
	}

	public void setFullStreamDownloaded(boolean isFullStreamDownloaded) {
		this.isFullStreamDownloaded = isFullStreamDownloaded;
	}
	
	
}
