package vessla.languages.downloader.task;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import vessla.languages.downloader.stream.info.StreamInfo;
import vessla.languages.downloader.task.event.MediaDownloadCompleteEvent;
import vessla.languages.downloader.task.event.MediaDownloadErrorEvent;
import vessla.languages.downloader.task.event.MediaDownloadEvent;

public class MediaDownloadTask implements Runnable{
	
	private static final Logger logger = Logger.getLogger(MediaDownloadTask.class);
		
	private String targetFileName;
	private String currentlyProcessedUrl;
	private StreamInfo downloadedStream;
	private ArrayList<MediaDownloadTaskListener> listeners;
	private RetryPolicy retryPolicy;
	private AbstractMediaDownloadTaskManager downloadManager;
	
	public MediaDownloadTask(AbstractMediaDownloadTaskManager downloadManager, StreamInfo downloadedStream, String targetFileName, MediaDownloadTaskListener downloadTaskListener) {
		this.downloadManager = downloadManager;
		this.downloadedStream = downloadedStream;
		this.targetFileName = targetFileName;
		this.listeners = new ArrayList<>();
		this.listeners.add(downloadTaskListener);
		this.retryPolicy = new RetryPolicy()
				.retryIf(result -> result instanceof MediaDownloadErrorEvent)
				.withBackoff(5, 30, TimeUnit.SECONDS)
				.withMaxRetries(2);
	}
	
	@Override
	public void run() {
		BigInteger mediaPartNumber = BigInteger.ZERO;
		while((this.currentlyProcessedUrl = downloadManager.getMediaPartUrl(downloadedStream, mediaPartNumber)) != null){
			MediaDownloadEvent downloadResult;
			logger.debug("Attempting to download part "+mediaPartNumber.intValue()+": "+currentlyProcessedUrl);
			if((downloadResult = Failsafe.with(retryPolicy).get(() -> download(currentlyProcessedUrl, targetFileName, true))) instanceof MediaDownloadCompleteEvent){
				notifyListeners(downloadResult);
				mediaPartNumber = mediaPartNumber.add(BigInteger.ONE);
			}
			else{
				notifyListeners(new MediaDownloadErrorEvent());
				return;
			}
		}
		notifyListeners(new MediaDownloadCompleteEvent(true));
	}
	
	public MediaDownloadEvent download(String urlString, String fileName, boolean fileAppend) {
		MediaDownloadEvent result = new MediaDownloadErrorEvent();
		try (ReadableByteChannel networkStreamChannel =  Channels.newChannel(new URL(urlString).openStream());
			 FileChannel partFileWriteChannel = new FileOutputStream(fileName+".part").getChannel();
			 FileChannel partFileReadChannel = new FileInputStream(fileName+".part").getChannel();
			 FileChannel mainDestFileChannel = new FileOutputStream(fileName, fileAppend).getChannel())
		{
			partFileWriteChannel.transferFrom(networkStreamChannel, 0, Long.MAX_VALUE);
			mainDestFileChannel.transferFrom(partFileReadChannel, mainDestFileChannel.size(), partFileReadChannel.size());
			result = new MediaDownloadCompleteEvent(false);
		} catch (FileNotFoundException e) {
			logger.debug(e);
		} catch (IOException e) {
			logger.debug(e);
		} finally {
			try{
				Files.delete(Paths.get(fileName+".part"));
			} catch (NoSuchFileException e) {
				logger.debug(e);
			} catch (DirectoryNotEmptyException e) {
				logger.debug(e);
			} catch (IOException e) {
			    // File permission problems are caught here.
				logger.debug(e);
			}
		}
		return result;
	}
	
	public void notifyListeners(MediaDownloadEvent e){
		for(MediaDownloadTaskListener listener:listeners){
			if(e instanceof MediaDownloadCompleteEvent){
				listener.onDownloadComplete((MediaDownloadCompleteEvent)e);
			}
			else{
				listener.onDownloadError((MediaDownloadErrorEvent)e);
			}
		}
	}
	
}
