package vessla.languages.downloader.task;

import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;

import vessla.languages.downloader.exception.DownloadInProgressException;
import vessla.languages.downloader.exception.UnableToFetchStreamInfoException;
import vessla.languages.downloader.stream.info.StreamInfo;
import vessla.languages.downloader.stream.info.StreamInfo.StreamType;

public interface MediaDownloadTaskManager {

	public void fetchStreamInfo(String videoUrl) throws UnableToFetchStreamInfoException;

	public ArrayList<StreamInfo> getAvailableStreams(StreamType type);
	
	public void download(StreamInfo streamInfo, String targetFile, MediaDownloadTaskListener listener) throws DownloadInProgressException, FileAlreadyExistsException;
	
	public boolean isDownloadInProgress();
	
}
