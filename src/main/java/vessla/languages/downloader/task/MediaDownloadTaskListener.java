package vessla.languages.downloader.task;

import vessla.languages.downloader.task.event.MediaDownloadCompleteEvent;
import vessla.languages.downloader.task.event.MediaDownloadErrorEvent;

public interface MediaDownloadTaskListener {
	
	public void onDownloadComplete(MediaDownloadCompleteEvent e);
	
	public void onDownloadError(MediaDownloadErrorEvent e);

}
