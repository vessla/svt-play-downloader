package vessla.languages.downloader.task;

import java.math.BigInteger;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import vessla.languages.downloader.exception.DownloadInProgressException;
import vessla.languages.downloader.stream.info.StreamInfo;
import vessla.languages.downloader.task.event.MediaDownloadCompleteEvent;
import vessla.languages.downloader.task.event.MediaDownloadErrorEvent;

public abstract class AbstractMediaDownloadTaskManager implements MediaDownloadTaskManager, MediaDownloadTaskListener{
	
	private static final Logger logger = Logger.getLogger(AbstractMediaDownloadTaskManager.class);
	
	private boolean isDownloadInProgress;
	protected String targetFileName;
	private ExecutorService downloadExecutorService;
	private MediaDownloadTaskListener downloadListener;
	
	@Override
	public void download(StreamInfo streamInfo, String targetFileName, MediaDownloadTaskListener listener) throws DownloadInProgressException, FileAlreadyExistsException {
		if(!isDownloadInProgress){
			//TODO: Future work - do not block multiple download calls, just add a new download task
			if(!Files.exists(Paths.get(targetFileName))){
				setDownloadInProgress(true);
				this.downloadListener = listener;
				this.targetFileName = targetFileName;
				downloadExecutorService = Executors.newFixedThreadPool(1);
				downloadExecutorService.execute(new MediaDownloadTask(this, streamInfo, targetFileName, this));
				downloadExecutorService.shutdown();
			}
			else{
				throw new FileAlreadyExistsException(targetFileName);
			}
		}
		else{
			throw new DownloadInProgressException();
		}
	}
	
	protected abstract String getMediaPartUrl(StreamInfo streamInfo, BigInteger partNumber);
	
	//-------------------------------------------------------------------------------------
	// Methods from MediaDownloadTaskListener interface
	//-------------------------------------------------------------------------------------
	
	@Override
	public void onDownloadComplete(MediaDownloadCompleteEvent e) {
		if(e.isFullStreamDownloaded()){
			logger.debug("Media stream download sucessfully completed!");
			setDownloadInProgress(false);
			downloadListener.onDownloadComplete(new MediaDownloadCompleteEvent(true));
		}
		else{
			logger.debug("Media part download sucessfully completed!");
		}
	}

	@Override
	public void onDownloadError(MediaDownloadErrorEvent e) {
		logger.debug("Media part download error...oops");
		setDownloadInProgress(false);
		downloadListener.onDownloadError(new MediaDownloadErrorEvent());
	}
	
	//-------------------------------------------------------------------------------------
	// Getters & setters
	//-------------------------------------------------------------------------------------
	
	public boolean isDownloadInProgress(){
		return this.isDownloadInProgress;
	}
	
	private void setDownloadInProgress(boolean isDownloadInProgress) {
		this.isDownloadInProgress = isDownloadInProgress;
	}

}
