package vessla.languages.downloader.svt;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import vessla.languages.downloader.exception.UnableToFetchStreamInfoException;
import vessla.languages.downloader.stream.description.mpd.AdaptationSetType;
import vessla.languages.downloader.stream.description.mpd.RepresentationType;
import vessla.languages.downloader.stream.description.mpd.SegmentTimelineType;
import vessla.languages.downloader.stream.description.mpd.MPDtype;
import vessla.languages.downloader.stream.info.MpegDashStreamInfo;
import vessla.languages.downloader.stream.info.StreamInfo;
import vessla.languages.downloader.stream.info.StreamInfo.StreamType;
import vessla.languages.downloader.stream.info.SubtitlesStreamInfo;
import vessla.languages.downloader.task.AbstractMediaDownloadTaskManager;

public class SvtMediaDownloadTaskManager extends AbstractMediaDownloadTaskManager{
	
	private static Logger logger = Logger.getLogger(SvtMediaDownloadTaskManager.class);
	
	private HashMap<StreamType, ArrayList<StreamInfo>> availableStreams;
	
	public SvtMediaDownloadTaskManager() {
		availableStreams = new HashMap<StreamType, ArrayList<StreamInfo>>();
		for(StreamType type: StreamType.values()){
			availableStreams.put(type, new ArrayList<StreamInfo>());
		} 
	}

	@Override
	public void fetchStreamInfo(String videoUrl) throws UnableToFetchStreamInfoException {
		JsonObject videoData = fetchVideoData(videoUrl);
		
		if(videoData != null){
			//TODO: Target file name is currently stored in MediaDownloadTask something more to replace? - move this part to Utils?
			//this.targetFileName = "svt_"+(videoData.get("programTitle")+"_"+ videoData.get("episodeTitle")).replaceAll("\\\"","").replaceAll(":", "");
		
			extractSubtitlesStreamInfo(videoData);
			URL mpdUrl = extractMpdUri(videoData);
			String baseUrl = mpdUrl.getProtocol()+"://"+mpdUrl.getHost()+mpdUrl.getPath();
			baseUrl = baseUrl.substring(0, baseUrl.lastIndexOf("/"));
		
			String mpdFile = fetchUriAsString(mpdUrl.toString());
			if(mpdFile != null){
				extractAudioVideoStreamInfo(mpdFile, baseUrl);
			} else{
				throw new UnableToFetchStreamInfoException();
			}
		} else{
			throw new UnableToFetchStreamInfoException();
		}
	}
	
	@Override
	public ArrayList<StreamInfo> getAvailableStreams(StreamType type) {
		return availableStreams.get(type);
	}

	@Override
	public String getMediaPartUrl(StreamInfo streamInfo, BigInteger partNumber) {
		return (streamInfo instanceof MpegDashStreamInfo)?getAudioVideoPartUrl(streamInfo, partNumber):getSubtitlesPartUrl((SubtitlesStreamInfo)streamInfo, partNumber);
	}
	
	private String getAudioVideoPartUrl(StreamInfo streamInfo, BigInteger partNumber){
		MpegDashStreamInfo mpegDashStreamInfo = (MpegDashStreamInfo)streamInfo;
		AdaptationSetType adaptationSet = mpegDashStreamInfo.getMpd().getPeriod().get(0).getAdaptationSet().get(mpegDashStreamInfo.getAdaptationSetIndex());
		RepresentationType representation = adaptationSet.getRepresentation().get(mpegDashStreamInfo.getRepresentationIndex());
		
		Pattern pattern = Pattern.compile("%0\\d+d");
	    Matcher partNumberingPatternMatcher = pattern.matcher(adaptationSet.getSegmentTemplate().getMedia());
	    
	    return (!(partNumber.compareTo(streamInfo.getPartCount())==1) && partNumberingPatternMatcher.find())?
	    		mpegDashStreamInfo.getBaseUrl()+"/"+adaptationSet.getSegmentTemplate().getMedia().replace("$RepresentationID$", representation.getId()).replaceAll("\\$Number%0\\d+d\\$", String.format (partNumberingPatternMatcher.group(), partNumber))
	    		:null;
	}
	
	private String getSubtitlesPartUrl(SubtitlesStreamInfo streamInfo, BigInteger partNumber){
		return partNumber.compareTo(streamInfo.getPartCount())==-1?((SubtitlesStreamInfo)streamInfo).getUrl():null;
	}
	
	//----------------------------------------------------------------------------------
	// Methods for extracting the required media information from the SVT Play website
	//----------------------------------------------------------------------------------
	
	private JsonObject fetchVideoData(String videoUrl){
		JsonObject result = null;
		
		try {
			Document mainWebsite = Jsoup.connect(videoUrl).get();
			Elements video = mainWebsite.select("video");
						
			String content = fetchUriAsString("https://api.svt.se/videoplayer-api/video/"+video.attr("data-video-id"));
			result = new JsonParser().parse(content).getAsJsonObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private static String fetchUriAsString(String uri) {
		String result = null;
		
		try {
			final int OK = 200;
			URL url = new URL(uri);
		
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			int responseCode = connection.getResponseCode();
			if(responseCode == OK){
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			
				result = response.toString();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			
		return result;
	}
	
	private URL extractMpdUri(JsonObject videoData){
		URL mpdUrl = null;
		JsonArray videoURLs = videoData.getAsJsonArray("videoReferences");
		for(JsonElement url:videoURLs){
			JsonObject videoUrlAsObject = url.getAsJsonObject();
			if(videoUrlAsObject.get("format").getAsString().equals("dashhbbtv")){
				try {
					mpdUrl = new URL(URLDecoder.decode(videoUrlAsObject.get("url").getAsString(), "UTF-8"));
				} catch (MalformedURLException | UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				break;
			}
		}
		return mpdUrl;
	}
	
	private void extractSubtitlesStreamInfo(JsonObject videoData){
		JsonArray subtitlesURLs = videoData.getAsJsonArray("subtitleReferences");
		for(JsonElement url:subtitlesURLs){
			JsonObject subtitlesUrlAsObject = url.getAsJsonObject();
			if(subtitlesUrlAsObject.get("format").getAsString().equals("webvtt")){
				try {
					availableStreams.get(StreamType.SUBTITLES).add(new SubtitlesStreamInfo("Format webvtt", StreamType.SUBTITLES, URLDecoder.decode(subtitlesUrlAsObject.get("url").getAsString(), "UTF-8"), BigInteger.valueOf(1)));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				break;
			}
		}
	}
	
	private void extractAudioVideoStreamInfo(String mpdFile, String baseUrl){
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Class.forName(MPDtype.class.getName()));
		
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			MPDtype mpd = (MPDtype)JAXBIntrospector.getValue(unmarshaller.unmarshal(new ByteArrayInputStream(mpdFile.getBytes())));
			
			AdaptationSetType currentAdaptationSet;
			RepresentationType currentRepresentation;
			BigInteger partCount;
			for(int i=0; i<mpd.getPeriod().get(0).getAdaptationSet().size(); i++){
				currentAdaptationSet = mpd.getPeriod().get(0).getAdaptationSet().get(i);
				partCount = BigInteger.ONE;
				for(SegmentTimelineType.S streamPart : currentAdaptationSet.getSegmentTemplate().getSegmentTimeline().getS()){
					partCount = partCount.add(streamPart.getR().compareTo(BigInteger.ZERO)==0?BigInteger.ONE:streamPart.getR());
				}
				logger.debug("Part count ("+i+"): "+partCount);
				for(int j=0; j<currentAdaptationSet.getRepresentation().size(); j++){
					currentRepresentation = currentAdaptationSet.getRepresentation().get(j);
					if(currentAdaptationSet.getContentType().equals("video")){
		    			availableStreams.get(StreamType.VIDEO).add(new MpegDashStreamInfo("height: "+currentRepresentation.getHeight()+" width: "+currentRepresentation.getWidth(), StreamType.VIDEO, mpd, i, j, baseUrl, partCount));
		    		}else{
		    			availableStreams.get(StreamType.AUDIO).add(new MpegDashStreamInfo("audio sampling rate: "+currentRepresentation.getAudioSamplingRate(), StreamType.AUDIO, mpd, i, j, baseUrl, partCount));
		    		}
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
}
