package vessla.languages.downloader;

import org.kohsuke.args4j.Option;

import vessla.languages.downloader.stream.info.StreamInfo.StreamType;

public class MediaDownloadCommandLineOptions {
	
	@Option(name = "-s", required = true, usage = "source stream URL")
    private String sourceStreamUrl;
	
	@Option(name = "-d", required = false, usage = "index of the stream to download", depends = {"-t", "-l"})
    private Integer streamIndex;
	
	@Option(name = "-t", required = false, usage = "name of the target file to download save the stream to", depends = "-d")
    private String targetFileName;
	
	@Option(name = "-l", required = false, usage = "type of the target stream")
    private StreamType streamType;
	
	//-----------------------------------------------------------------------------------------
	// Getters and setters
	//-----------------------------------------------------------------------------------------

	public String getSourceStreamUrl() {
		return sourceStreamUrl;
	}

	public void setSourceStreamUrl(String sourceStreamUrl) {
		this.sourceStreamUrl = sourceStreamUrl;
	}

	public String getTargetFileName() {
		return targetFileName;
	}

	public void setTargetFileName(String targetFileName) {
		this.targetFileName = targetFileName;
	}

	public StreamType getStreamType() {
		return streamType;
	}

	public void setStreamType(StreamType streamType) {
		this.streamType = streamType;
	}

	public Integer getStreamIndex() {
		return streamIndex;
	}

	public void setStreamIndex(Integer streamIndex) {
		this.streamIndex = streamIndex;
	}
	
}
