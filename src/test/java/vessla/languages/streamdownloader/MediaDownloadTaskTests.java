package vessla.languages.streamdownloader;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigInteger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import vessla.languages.downloader.stream.info.MpegDashStreamInfo;
import vessla.languages.downloader.svt.SvtMediaDownloadTaskManager;
import vessla.languages.downloader.task.MediaDownloadTask;
import vessla.languages.downloader.task.MediaDownloadTaskListener;

@RunWith(MockitoJUnitRunner.class)
public class MediaDownloadTaskTests {
	
	@Mock
	private MpegDashStreamInfo downloadedStreamInfo;
	
	@Mock
	MediaDownloadTaskListener downloadListener;
	
	@Mock
	SvtMediaDownloadTaskManager downloadManager;
	
	@Test
	public void listenersOnDownloadErrorInvokedWhenConnectionError() {
		doReturn("malformedurl").when(downloadManager).getMediaPartUrl(downloadedStreamInfo, BigInteger.ZERO);
		
		MediaDownloadTask downloadTask = new MediaDownloadTask(downloadManager, downloadedStreamInfo, "test.m4a", downloadListener);
		downloadTask.run();
		
		verify(downloadListener, only()).onDownloadError(any());
	}
	
	@Test
	public void downloadRetriedWhenConnectionError() {
		doReturn("malformedurl").when(downloadManager).getMediaPartUrl(downloadedStreamInfo, BigInteger.ZERO);
		
		MediaDownloadTask downloadTask = spy(new MediaDownloadTask(downloadManager, downloadedStreamInfo, "test.m4a", downloadListener));
		downloadTask.run();
		
		verify(downloadTask, times(3)).download(downloadManager.getMediaPartUrl(downloadedStreamInfo, BigInteger.ZERO), "test.m4a", true);
	}

}
