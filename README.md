# README #

### What is this repository for? ###

This application has been created for private use, as a tool to support language learning process. It allows to archive the content which is available via the [SVTPlay](https://www.svtplay.se/) website for a limited amount of time (along with subtitles).

The application was designed to be as extensible as possible in terms of the possibility of replacing the default stream information parser with the new ones (which support other streaming services). The class providing a new parser only needs to extend AbstractMediaDownloadTaskManager class and implement three methods associated with extracting stream information from this particular website, and dynamically generating URLs for each stream part.


The current version of the parser has only been tested on the videos, which are available in all countries (regardless the origin IP address). 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND

### How do I get set up? ###

Standalone jar with all the required dependencies can be built using the following Maven command:
```
mvn clean compile assembly:single install
```

External libraries used in the project:

1. Logging: [Log4j](https://logging.apache.org/log4j/2.x/)
2. HTML parsing: [Jsoup](https://jsoup.org/)
3. JSON deserialization: [Gson](https://github.com/google/gson)
4. Parsing command line arguments: [Args4j](http://args4j.kohsuke.org/)
5. Retry stream part download on network failure: [Failsafe](https://github.com/jhalterman/failsafe)
6. Testing: [JUnit](https://junit.org/junit4/), [Mockito](http://site.mockito.org/)

Schema for generating Java classes used to deserialize data from the stream description (.mpd) file is available [here](https://bitbucket.org/vessla/svtplaydownloader/src/22dcdeb74767d391a493fa43ec0b615d2498035b/xsd/?at=master). Schema has been downloaded from an [external repository](https://github.com/Dash-Industry-Forum/Conformance-and-reference-source/blob/master/conformance/MPDValidator/schemas/DASH-MPD.xsd)

#### Running the application ####

Parameters:
```

-s [url]			(required) URL of the website containing the requested stream
-l [stream type]	type of the stream user wants to download or display detailed information about (accepted values: AUDIO, VIDEO, AUDIOVIDEO, SUBTITLES)
-d [index]			index of the stream to download (on the list of the streams of a specified type)
-t [file name]		name of the file to download the stream to

```

Getting information about all available stream types:
```
java -jar svtdownloader-standalone.jar -s https://www.svtplay.se/video/16016419/vetenskapens-varld/vetenskapens-varld-sasong-27-magasin-nobelpris-och-ljudvetenskap?start=auto&tab=2017 
```
Getting information about a specific stream type:
```
java -jar svtdownloader-standalone.jar -s https://www.svtplay.se/video/16016419/vetenskapens-varld/vetenskapens-varld-sasong-27-magasin-nobelpris-och-ljudvetenskap?start=auto&tab=2017 -l VIDEO
```

Downloading a specific video stream:
```
java -jar svtdownloader-standalone.jar -s https://www.svtplay.se/video/16016419/vetenskapens-varld/vetenskapens-varld-sasong-27-magasin-nobelpris-och-ljudvetenskap?start=auto&tab=2017 -l VIDEO -d 0 -t video.m4v
```

Downloading subtitles:
```
java -jar svtdownloader-standalone.jar -s https://www.svtplay.se/video/16016419/vetenskapens-varld/vetenskapens-varld-sasong-27-magasin-nobelpris-och-ljudvetenskap?start=auto&tab=2017 -l SUBTITLES -d 0 -t subtitles.vtt
```

WARNING: Current version of the application does not support muxing audio and video files. This can be achieved with [FFmpeg](https://www.ffmpeg.org/) and the following command: 
```
ffmpeg -i video.m4v -i audio.m4a -c:v copy -c:a copy output.mp4
```

### Useful links regarding MPD structure ###

1. [The structure of an MPEG-DASH MPD](https://www.brendanlong.com/the-structure-of-an-mpeg-dash-mpd.html)
2. [Number of segments in a DASH MPD](https://stackoverflow.com/questions/21634622/number-of-segments-in-a-dash-mpd)
3. [Stop numbering: The underappreciated power of DASH's SegmentTimeline](http://www.unified-streaming.com/blog/stop-numbering-underappreciated-power-dashs-segmenttimeline)
4. [How to play mpd file](https://stackoverflow.com/questions/31942278/how-to-play-mpd-file)
5. [Online player](http://players.akamai.com/dash/)

### Who do I talk to? ###

pjadamska@gmail.com